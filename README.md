Pypestream Test Task
====================

Application requires Python 3.6 or higher.

The system consist of two small Flask-based dev microservices and py.test
test that verifies behavior of both microservices.

The first microservice is called Constructor microservice.
It has one endpoint (`GET /addresses`) that returns XML response. Under
the hood this microcervice parses source files stored in subdirectory
`data/` and creates XML tree from it.

The second microservice is called Saver microservice.
It also has one endpoint (`PUT /addresses`) that expects valid XML string
to be provided and then saves it into the file in `results/` subdirectory.

Test script uses fixtures of test clients of both microservices and
sequentially makes request to both microservices, verifies response
status code and checks if the .xml file has appeared in the result
directory.
## Getting Started

```bash
$ cd pypestream-anatoliy
$ python3.6 -m venv env
(env) $ . env/bin/activate
(env) $ make install_dependencies
```

### Running python test

This script uses Flask test clients of both microservices and makes
requests to both of them. So that application logic can be tested
without running of actual microservices.

```bash
$ make pytest
```

### Running microservices

This command will run both microservices in a background.
```bash
$ make run_services
```

### Running integration test

This script requires both microservices to be running.
Scripts makes GET request to the first microservice and sends the
response as the body for PUT request to the second microservice.
Then script check presence of XML file in results directory.

```bash
$ make test_script
```
