# Add any tasks that are not dependent on files to the .PHONY list.
.PHONY: lint

lint:
	flake8 services/

install_dependencies:
	pip install -U pip
	pip install -r requirements.txt
	pip install -r services/constructor/requirements.txt
	pip install -r services/saver/requirements.txt

run_services:
	PYTHONPATH=$(PWD) python services/constructor/application.py &
	PYTHONPATH=$(PWD) python services/saver/application.py &

test_script:
	PYTHONPATH=$(PWD) python tests/integration.py

pytest:
	PYTHONPATH=$(PWD) py.test tests/test_script.py
