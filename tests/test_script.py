import json
import os

import pytest

from services.constructor import config as constructor_config
from services.constructor.application import app as constructor_app
from services.saver import config as saver_config
from services.saver.application import app as saver_app


@pytest.fixture
def constructor_client():
    """Return flask test client.

    Returns:
        flask client: flask test client
    """
    return constructor_app.test_client()


@pytest.fixture
def saver_client():
    """Return flask test client.

    Returns:
        flask client: flask test client
    """
    return saver_app.test_client()


def test_microservices_integration(constructor_client, saver_client):
    """Test constructor microservice returns XML and saver saves it to file."""
    get_xml_url = 'http://{0}:{1}/addresses'.format(
        constructor_config.APPLICATION_HOST,
        constructor_config.APPLICATION_PORT)

    # Url of Constructor microservice endpoint that returns XML data.
    get_xml_response = constructor_client.get(get_xml_url)
    assert get_xml_response.status_code == 200

    # Url of Saver microservice endpoint that saves XML into file.
    put_xml_url = 'http://{0}:{1}/addresses'.format(
        saver_config.APPLICATION_HOST, saver_config.APPLICATION_PORT)
    put_xml_response = saver_client.put(
        put_xml_url, data=get_xml_response.data)
    assert put_xml_response.status_code == 200

    put_xml_response_json = json.loads(put_xml_response.data.decode('utf-8'))
    assert os.path.isfile(put_xml_response_json['filename'])
