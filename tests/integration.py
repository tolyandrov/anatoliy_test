"""Script makes sequence requests to running microservices.

In order to run this script you should run microservices first since it
makes real requests to microservices URLs. If you want to use flask test
clients instead of running real microservices you can use test_script.py.
"""

import requests
from coverage.annotate import os

from services.constructor import config as constructor_config
from services.saver import config as saver_config

get_xml_url = 'http://{0}:{1}/addresses'.format(
    constructor_config.APPLICATION_HOST,
    constructor_config.APPLICATION_PORT)

# Url of Constructor microservice endpoint that returns XML data.
get_xml_response = requests.get(get_xml_url)

# Url of Saver microservice endpoint that saves XML into file.
put_xml_url = 'http://{0}:{1}/addresses'.format(
    saver_config.APPLICATION_HOST, saver_config.APPLICATION_PORT)
put_xml_response = requests.put(
    put_xml_url, data=get_xml_response.content)

assert get_xml_response.status_code == 200
assert put_xml_response.status_code == 200
assert os.path.isfile(put_xml_response.json()['filename'])

