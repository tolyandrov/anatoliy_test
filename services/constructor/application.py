"""Flask application for constructor microservice and dev server."""
from flask import Flask

from services.constructor import config

app = Flask(__name__)
app.logger.addHandler(config.FILE_HANDLER_LOGGER)

from services.constructor.handlers import *  # noqa

if __name__ == '__main__':
    app.run(port=config.APPLICATION_PORT)
