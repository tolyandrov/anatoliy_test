"""Errors/exceptions related constants."""

INVALID_DATA_IN_ROW = (
    '{filename}: invalid data in a row {row} can\'t be converted to XML.'
    ' Row is skipped')
