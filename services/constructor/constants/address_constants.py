"""Constants for parses logic module."""

# Address fields
ADDRESS = 'address'
CITY = 'city'
COUNTRY = 'country'
STATE = 'state'
ZIP = 'zip'
ZIP_CODE = 'zip_code'
SOURCE_FIELDS_TO_ATTRIBUTES_MAPPING = {
    CITY: CITY,
    COUNTRY: COUNTRY,
    STATE: STATE,
    ZIP: ZIP_CODE}
ALLOWED_ADDRESS_FIELDS = {CITY, COUNTRY, STATE, ZIP}

# Source data related constants
ATTRIBUTE_SEPARATOR = ';'
VALUE_SEPARATOR = '='
