"""Logic module responsible for parsing raw data and constructing XML."""

import os

from lxml import etree

from services.constructor import config
from services.constructor.application import app
from services.constructor.constants import errors
from services.constructor.models.address import Address


logger = app.logger


def extract_xml_from_file(filename):
    """Read a file and extract XML objects from it.

    Args:
        filename (str): name of file that should be parsed.

    Returns:
        list: list of XML objects from file.

    """
    file_xml_objects = []
    with open(filename) as fl:
        for number, line in enumerate(fl.readlines()):
            try:
                line_xml = Address(line).to_xml()
                file_xml_objects.append(line_xml)
            except (AttributeError, ValueError):
                logger.warning(errors.INVALID_DATA_IN_ROW.format(
                    filename=filename, row=number))
                continue
    return file_xml_objects


def parse_addresses_data():
    """Read all files sequentially and return combined XML string.

    Returns:
        str: string with aggregated xml data from static files.

    """
    files_xmls = []
    files = sorted(os.listdir(config.SOURCE_FILES_PATH))
    for filename in files:
        file_path = os.path.join(config.SOURCE_FILES_PATH, filename)
        files_xmls.extend(extract_xml_from_file(file_path))
    root = etree.Element('root')
    for xml in files_xmls:
        root.append(xml)
    return etree.tostring(root, encoding='utf-8', xml_declaration=True)
