"""Descriptors models are described here."""


class Descriptor:
    """Base descriptor class."""

    def __set_name__(self, owner, name):
        """Set the attribute name as a descriptor name."""
        self.name = name


class String(Descriptor):
    """Descriptor that allow only string values to be used in attribute."""

    def __set__(self, instance, value):
        """Validate and set provided value.

        Apply validation rules of this and all parent descriptors.
        If no errors were raised, set the value to an instance.

        Args:
            instance (object): instance of the owner class.
            value (str): the value to set the descriptor to.

        Raises:
            ValueError: raised if invalid (not allowed) data type was provided.

        """
        if not isinstance(value, str):
            raise ValueError('{0} should be string, not {1}.'.format(
                self.name, type(value)))
        instance.__dict__[self.name] = value


class AlphaString(String):
    """Descriptor allows only alphabetical string values to be used in attr."""

    def __set__(self, instance, value):
        """Validate and set provided value.

        Apply validation rules of this and all parent descriptors.
        If no errors were raised, set the value to an instance.

        Args:
            instance (object): instance of the owner class.
            value (str): the value to set the descriptor to.

        Raises:
            ValueError: raised if invalid (not allowed) data type was provided.

        """
        super().__set__(instance, value)
        if not value.isalpha():
            raise ValueError(
                '{0} should contain alphabetic characters only.'.format(
                    self.name))


class DigitString(String):
    """Descriptor that allows only digit string values to be used in attr."""

    def __set__(self, instance, value):
        """Validate and set provided value.

        Apply validation rules of this and all parent descriptors.
        If no errors were raised, set the value to an instance.

        Args:
            instance (object): instance of the owner class.
            value (str): the value to set the descriptor to.

        Raises:
            ValueError: raised if invalid (not allowed) data type was provided.

        """
        super().__set__(instance, value)
        if not value.isdigit():
            raise ValueError('{0} should contain only digits.'.format(
                self.name))
