"""Class represents Address, allows to parse and validate raw data."""

from lxml import etree

from services.constructor.application import app
from services.constructor.constants import address_constants
from services.constructor.models.descriptors import AlphaString
from services.constructor.models.descriptors import DigitString


logger = app.logger


class Address:
    """Class provides interface for parsing and verifying raw address data.

    Instance should be initialized with one string of raw addresses data.
    This string will be parsed according to predefined pattern and extracted
    key-value pairs will be saved as public attributes.
    Each attribute is a descriptor that has some data validation on setting up.
    In case of violation of attributes rules, the violation will be logged
    and value error will be raised.

    Attributes:
        country (String): country code.
        city (String): full name of a city in lowercase.
        state (String): state code.
        zip_code (String): zip code.

    """

    country = AlphaString()
    city = AlphaString()
    state = AlphaString()
    zip_code = DigitString()

    def __init__(self, source_string):
        """Init Address instance with a raw string of address data.

        Args:
            source_string (str): semicolon separated address attributes in
                format "attribute1=value1;attribute2=value2".
        """
        # Save source string without newline character.
        self.source_string = source_string.replace('\n', '')
        self._parse_source_data()

    def _parse_source_data(self):
        """Parse source string and set its fields as instance attributes."""
        address_record = self.source_string.split(
            address_constants.ATTRIBUTE_SEPARATOR)
        for field in address_record:
            key, value = field.split(address_constants.VALUE_SEPARATOR)
            if key not in address_constants.ALLOWED_ADDRESS_FIELDS:
                raise AttributeError

            attribute_name = (
                address_constants.SOURCE_FIELDS_TO_ATTRIBUTES_MAPPING.get(key))
            try:
                self.__setattr__(attribute_name, value)
            except ValueError as ex:
                logger.debug(ex)
                raise

    def to_xml(self):
        """Create lxml object from an Address attributes.

        Returns:
            etree.Element: lxml object contains XML created from source line.

        """
        address = etree.Element(address_constants.ADDRESS)
        # Iterate through attributes and create XML tags from allowed ones.
        attributes = vars(self)
        for field, attribute_name in (
                address_constants.SOURCE_FIELDS_TO_ATTRIBUTES_MAPPING.items()):
            value = attributes.get(attribute_name)
            child = etree.Element(field)
            child.text = value
            address.append(child)
        return address
