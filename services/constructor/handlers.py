"""Constructor mircroservice handlers."""

from flask import Response

from services.constructor.application import app
from services.constructor.logic import parser


@app.errorhandler(500)
def exception_handler(error):
    """Handle uncaught exception.

    Returns:
        flask.Response: A 500 response with custom error message.

    """
    message = (
        'The server encountered an internal error '
        'and was unable to complete your request.')
    app.logger.error(error)
    return Response(message)


@app.route('/addresses', methods=['GET'])
def get_addresses():
    """Handle client's GET request and return aggregated XML.

    Returns:
        Response: aggregated XML in text/xml mimetype response.

    """
    result = parser.parse_addresses_data()
    return Response(result, mimetype='text/xml')
