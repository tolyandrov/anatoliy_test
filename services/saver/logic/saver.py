"""Logic module responsible for saving XML data to files."""

from services.saver import config
from services.saver.application import app

logger = app.logger


def save_xml_to_file(xml_data):
    """Save received xml to .xml file.

    Args:
        xml_data (bytes): valid XML data.

    Returns:
        int: status of saving operation.

    """
    try:
        with open(config.SAVE_XML_PATH, 'wb') as fl:
            fl.write(xml_data)
            logger.info('New XML file was saved.')
        return {'status': 200, 'filename': config.SAVE_XML_PATH}
    except FileNotFoundError as ex:
        logger.exception(ex)
