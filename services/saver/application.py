"""Flask application for saver microservice and dev server."""

from flask import Flask

from services.saver import config

app = Flask(__name__)
app.logger.addHandler(config.FILE_HANDLER_LOGGER)

from services.saver.handlers import *  # noqa

if __name__ == '__main__':
    app.run(host=config.APPLICATION_HOST, port=config.APPLICATION_PORT)
