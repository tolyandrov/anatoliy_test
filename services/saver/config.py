"""Microservice configuration."""

import logging
from logging.handlers import RotatingFileHandler
from os.path import abspath
from os.path import dirname
from os.path import join

# Configure logger
FILE_HANDLER_LOGGER = RotatingFileHandler(
    abspath(join(dirname(__file__), 'logs/saver.log')),
    maxBytes=1024 * 1024 * 100, backupCount=1)
FILE_HANDLER_LOGGER.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
FILE_HANDLER_LOGGER.setFormatter(formatter)

# Configure application
SAVE_XML_PATH = abspath(join(dirname(__file__), 'result/result.xml'))
APPLICATION_HOST = '0.0.0.0'
APPLICATION_PORT = 5000
