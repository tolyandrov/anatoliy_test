"""Constructor mircroservice handlers."""

from flask import jsonify
from flask import request
from flask import Response

from services.saver.application import app
from services.saver.logic import saver


@app.errorhandler(500)
def exception_handler(error):
    """Handle uncaught exception.

    Returns:
        flask.Response: A 500 response with a custom error message.

    """
    message = (
        'The server encountered an internal error '
        'and was unable to complete your request.')
    app.logger.error(error)
    return Response(message)


@app.route('/addresses', methods=['PUT'])
def get_addresses():
    """Handle client's PUT request and save aggregated XML from body to file.

    Returns:
        Response: response with status of operation.

    """
    data = request.get_data()
    result = saver.save_xml_to_file(data)
    return jsonify(result)
